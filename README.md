# Quick summary #
Change any photo/portrait into a masterpiece as if it would be painted by a famous painter.

It is 15 seconds of fame [1] spin off. Museum of modern art kind of installation. 
With a help of convolutional neural network models of many known artist pictures it can transform a visitor's portrait, captured by USB camera, into a new art. Visitor so to say become famous for 30 seconds, after that time new visitor is randomly selected and his/her portrait displayed. 


# Hardware #
- big (wooden) photo frame 
- USB, HD camera- screen with HDMI-in (e.g. 22") 
- Raspberry PI (for recording pictures via cam, displaying artificially modified visitor's portraits via HDMI screen, data transfer to/from computer via WiFi/Ethernet)
- Computer with decent hardware to run our (pretty resource hungry) application for artificially modifying pictures.


# Software #
Main part of the application is written in Python. For face detection it uses OpenCV framework (Open Source Computer Vision Library: http://opencv.org). Already learned CNN models are based on [5,6], of which theoretical backgrounds are [3,4]. To guarantee almost infinitive space of possible portrait styles and colors it uses CNN models as well as my custom color pattern schemas (sometimes portraits looks like they are pop-arts).


# Pictures #

Face detection and cut of portrait are marked
![photo 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_Rms3WGNGcUlLcVk&export=download)


A gallery of images based on which the currently used CNN models have been learned.
![photo 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_TlM1aVp6WTBiQ00&export=download)


Gallery of original image transformations based on CNN models - changed colors and styles.
![photo 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_dU9qX2lhaTRhM2s&export=download)


Gallery of original image transformations based on CNN models - only styles changed, colors are from the original input image.
![photo 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_VW1nbGlCdkQtRmc&export=download)


Image gallery processed with CNN models (only transformation of style, colors are from the original input image) and subsequently also with our color transformation algorithm.
![photo 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_Mm9sbk13a25kMWc&export=download)


Image gallery processed with CNN models (transformation of style and color) and subsequently with our color transformation algorithm.
![photo 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_ZEJnb2wxSGI4TlE&export=download)


Image gallery processed with randomly chosen combination of CNN model and color palette for color transformation.
![photo 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_U3liWU16Sk9wRDA&export=download)



Demo of art installation [2]

![photo 1] (https://drive.google.com/uc?id=0B1yfSN5YNf0_ZjJmNkEtV3RvcUU&export=download)



# References #

1. Solina F.: 15 seconds of fame, http://eprints.fri.uni-lj.si/129/1/solina_leonardo_04.pdf. Leonardo 37(2):105-110+125 (2004).

2. Solina F.: 15 sekund slave in virtualno smucanje / 15 Seconds of Fame and Virtual Skiing. Exhibition Catalogue, http://eprints.fri.uni-lj.si/194/. ArtNetLab,Ljubljana (2005). 

3. Leon A. Gatys, Alexander S. Ecker, Matthias Bethge: A Neural Algorithm of Artistic Style, https://arxiv.org/abs/1508.06576. Cornell University (2015).

4. Johnson J., Alahi A., Fei-Fei L.: Perceptual Losses for Real-Time Style Transfer and Super-Resolution, https://arxiv.org/abs/1603.08155. Cornell University (2016).

5. Tomoto Y.: chainer-fast-neuralstyle, https://github.com/yusuketomoto/chainer-fast-neuralstyle. GitHub repository.

6. Dizzle F.: chainer-fast-neuralstyle-models, https://github.com/gafr/chainer-fast-neuralstyle-models. GitHub repository.

7. Rosenberg C.: The Lenna Story - www.lenna.org, http://www.cs.cmu.edu/~chuck/lennapg/lenna.shtml.


(The code will be published soon, I need a little time to clean up all internal things, meantime you can contact me and  hopefully I will provide you the code)